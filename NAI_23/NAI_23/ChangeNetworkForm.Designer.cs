﻿namespace NAI_23
{
    partial class ChangeNetworkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fLNeuronAmountTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fLFunUniRadioButton = new System.Windows.Forms.RadioButton();
            this.fLFunBiRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sLFunUniRadioButton = new System.Windows.Forms.RadioButton();
            this.sLFunBiRadioButton = new System.Windows.Forms.RadioButton();
            this.sLNeuronAmountTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lambdaTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.learningRateTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ilość neuronów";
            // 
            // fLNeuronAmountTextBox
            // 
            this.fLNeuronAmountTextBox.Location = new System.Drawing.Point(91, 23);
            this.fLNeuronAmountTextBox.Name = "fLNeuronAmountTextBox";
            this.fLNeuronAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.fLNeuronAmountTextBox.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fLFunUniRadioButton);
            this.groupBox1.Controls.Add(this.fLFunBiRadioButton);
            this.groupBox1.Controls.Add(this.fLNeuronAmountTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(201, 97);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametry pierwszej warstwy";
            // 
            // fLFunUniRadioButton
            // 
            this.fLFunUniRadioButton.AutoSize = true;
            this.fLFunUniRadioButton.Location = new System.Drawing.Point(9, 72);
            this.fLFunUniRadioButton.Name = "fLFunUniRadioButton";
            this.fLFunUniRadioButton.Size = new System.Drawing.Size(173, 17);
            this.fLFunUniRadioButton.TabIndex = 3;
            this.fLFunUniRadioButton.Text = "Funkcja sigmoidalna unipolarna";
            this.fLFunUniRadioButton.UseVisualStyleBackColor = true;
            // 
            // fLFunBiRadioButton
            // 
            this.fLFunBiRadioButton.AutoSize = true;
            this.fLFunBiRadioButton.Checked = true;
            this.fLFunBiRadioButton.Location = new System.Drawing.Point(9, 49);
            this.fLFunBiRadioButton.Name = "fLFunBiRadioButton";
            this.fLFunBiRadioButton.Size = new System.Drawing.Size(167, 17);
            this.fLFunBiRadioButton.TabIndex = 2;
            this.fLFunBiRadioButton.TabStop = true;
            this.fLFunBiRadioButton.Text = "Funkcja sigmoidalna bipolarna";
            this.fLFunBiRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sLFunUniRadioButton);
            this.groupBox2.Controls.Add(this.sLFunBiRadioButton);
            this.groupBox2.Controls.Add(this.sLNeuronAmountTextBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 115);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 97);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parametry drugiej warstwy";
            // 
            // sLFunUniRadioButton
            // 
            this.sLFunUniRadioButton.AutoSize = true;
            this.sLFunUniRadioButton.Location = new System.Drawing.Point(9, 72);
            this.sLFunUniRadioButton.Name = "sLFunUniRadioButton";
            this.sLFunUniRadioButton.Size = new System.Drawing.Size(173, 17);
            this.sLFunUniRadioButton.TabIndex = 3;
            this.sLFunUniRadioButton.Text = "Funkcja sigmoidalna unipolarna";
            this.sLFunUniRadioButton.UseVisualStyleBackColor = true;
            // 
            // sLFunBiRadioButton
            // 
            this.sLFunBiRadioButton.AutoSize = true;
            this.sLFunBiRadioButton.Checked = true;
            this.sLFunBiRadioButton.Location = new System.Drawing.Point(9, 49);
            this.sLFunBiRadioButton.Name = "sLFunBiRadioButton";
            this.sLFunBiRadioButton.Size = new System.Drawing.Size(167, 17);
            this.sLFunBiRadioButton.TabIndex = 2;
            this.sLFunBiRadioButton.TabStop = true;
            this.sLFunBiRadioButton.Text = "Funkcja sigmoidalna bipolarna";
            this.sLFunBiRadioButton.UseVisualStyleBackColor = true;
            this.sLFunBiRadioButton.CheckedChanged += new System.EventHandler(this.sLFunBiRadioButton_CheckedChanged);
            // 
            // sLNeuronAmountTextBox
            // 
            this.sLNeuronAmountTextBox.Enabled = false;
            this.sLNeuronAmountTextBox.Location = new System.Drawing.Point(91, 23);
            this.sLNeuronAmountTextBox.Name = "sLNeuronAmountTextBox";
            this.sLNeuronAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.sLNeuronAmountTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ilość neuronów";
            // 
            // lambdaTextBox
            // 
            this.lambdaTextBox.Location = new System.Drawing.Point(146, 218);
            this.lambdaTextBox.Name = "lambdaTextBox";
            this.lambdaTextBox.Size = new System.Drawing.Size(57, 20);
            this.lambdaTextBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Współczynnik lambda";
            // 
            // learningRateTextBox
            // 
            this.learningRateTextBox.Location = new System.Drawing.Point(146, 244);
            this.learningRateTextBox.Name = "learningRateTextBox";
            this.learningRateTextBox.Size = new System.Drawing.Size(57, 20);
            this.learningRateTextBox.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Stała szybkości uczenia";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(138, 270);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(57, 270);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 11;
            this.acceptButton.Text = "OK";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // ChangeNetworkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 307);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.learningRateTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lambdaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ChangeNetworkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New network";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fLNeuronAmountTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton fLFunUniRadioButton;
        private System.Windows.Forms.RadioButton fLFunBiRadioButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton sLFunUniRadioButton;
        private System.Windows.Forms.RadioButton sLFunBiRadioButton;
        private System.Windows.Forms.TextBox sLNeuronAmountTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lambdaTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox learningRateTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button acceptButton;
    }
}