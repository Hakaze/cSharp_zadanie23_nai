﻿using NAI_23.DataF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAI_23.Network
{
    public class TwoLayerNetwork
    {
        public Data allData { get; set; }
        public Data dataForLearning { get; set; }
        public Neuron[] firstLayer { get; set; }
        public Neuron[] secondLayer { get; set; }
        public double learningRate { get; set; }
        public TwoLayerNetwork(Data allData)
        {
            this.allData = allData;
            //dataForLearning = new Data(allData);
            firstLayer = new Neuron[allData.firstLayerSize];
            secondLayer = new Neuron[allData.secondLayerSize];

            for(int i=0; i< firstLayer.Length; i++)
            {
                if (allData.networkLoaded)
                {
                    firstLayer[i] = new Neuron(allData.functionForFirstLayer, allData.lambda, allData.firstLayerWeights[i]);
                }
                else
                {
                    firstLayer[i] = new Neuron(allData.inputSize, allData.functionForFirstLayer, allData.lambda);
                }
                
            }
            for (int i = 0; i < secondLayer.Length; i++)
            {
                if (allData.networkLoaded)
                {
                    secondLayer[i] = new Neuron(allData.functionForSecondLayer, allData.lambda, allData.secondtLayerWeights[i]);
                }
                else
                {
                    secondLayer[i] = new Neuron(allData.firstLayerSize + 1, allData.functionForSecondLayer, allData.lambda);
                }
                
            }
            allData.normalizeData();
            for (int i=0; i<allData.data.Count; i++)
            {
                allData.data[i].firstLayerOutput = new double[allData.firstLayerSize+1];
                allData.data[i].secondLayerOutput = new double[allData.secondLayerSize];
            }
            learningRate = allData.learningRate;
            switch (allData.functionForSecondLayer)
            {
                case 0:
                    allData.minForOutput = -1;
                    allData.medForOutput = 0;
                    break;
                case 1:
                    allData.minForOutput = 0;
                    allData.medForOutput = 0.5;
                    break;

            }
        }


        public void calcFirstLayerAnwser()
        {
            for(int i=0; i< allData.data.Count; i++)
            {
                for(int j=0; j< firstLayer.Length; j++)
                {
                    allData.data[i].firstLayerOutput[j] = firstLayer[j].calcNeuronAnwser(allData.data[i].inputVector);
                }
                // dla 2 n
                allData.data[i].firstLayerOutput[firstLayer.Length] = 1;
            }
        }
        public void calcSecondLayerAnwser()
        {
            for (int i = 0; i < allData.data.Count; i++)
            {
                for (int j = 0; j < secondLayer.Length; j++)
                {
                    allData.data[i].secondLayerOutput[j] = secondLayer[j].calcNeuronAnwser(allData.data[i].firstLayerOutput);
                }
            }
        }
        public double calcValidity()
        {
            calcFirstLayerAnwser();
            calcSecondLayerAnwser();
            int correctSum = 0;
            for (int i = 0; i < allData.data.Count; i++)
            {
                Data.Pattern pattern = allData.data.ElementAt(i);
                bool wrong = false;
                for (int j = 0; j < pattern.secondLayerOutput.Length; j++)
                {
                    if (pattern.secondLayerOutput[j] >= allData.medForOutput && pattern.desiredOutput[j] == allData.minForOutput)
                    {
                        wrong = true;
                        break;
                    }
                }
                if (!wrong)
                {
                    correctSum++;
                }

            }
            return Math.Round((double)correctSum / (double)allData.data.Count, 2) * 100;
        }
        public void singleStepLerning()
        {
            /*
            if (dataForLearning.data.Count == 0)
            {
                dataForLearning = new Data(allData);
            }
            */
            dataForLearning = new Data(allData);
            while (dataForLearning.data.Count > 0)
            {


                int index = RandomNumberGenerator.randomInt(0, dataForLearning.data.Count);
                Data.Pattern pattern = dataForLearning.data.ElementAt(index);
                for (int j = 0; j < firstLayer.Length; j++)
                {
                    pattern.firstLayerOutput[j] = firstLayer[j].calcNeuronAnwser(pattern.inputVector);
                }
                // dla 2 n
                pattern.firstLayerOutput[firstLayer.Length] = 1;
                for (int j = 0; j < secondLayer.Length; j++)
                {
                    pattern.secondLayerOutput[j] = secondLayer[j].calcNeuronAnwser(pattern.firstLayerOutput);
                }
                double[] errorSL = new double[secondLayer.Length];
                for (int j = 0; j < secondLayer.Length; j++)
                {
                    errorSL[j] = secondLayer[j].calcDeriv() * (pattern.desiredOutput[j] - secondLayer[j].lastAnwser);
                }
                double[] errorFL = new double[firstLayer.Length];
                for (int j = 0; j < firstLayer.Length; j++)
                {
                    errorFL[j] = 0;
                    for (int i = 0; i < secondLayer.Length-1; i++)
                    {
                        errorFL[j] = errorFL[j] + errorSL[i] * secondLayer[i].weights[j];
                    }
                    errorFL[j] = errorFL[j] * firstLayer[j].calcDeriv();
                }

                for (int j = 0; j < secondLayer.Length; j++)
                {
                    for (int i = 0; i < secondLayer[j].weights.Length; i++)
                    {
                        secondLayer[j].weights[i] = secondLayer[j].weights[i] + learningRate * errorSL[j] * pattern.firstLayerOutput[i]; ;
                    }
                }
                for (int j = 0; j < firstLayer.Length; j++)
                {
                    for (int i = 0; i < firstLayer[j].weights.Length; i++)
                    {
                        firstLayer[j].weights[i] = firstLayer[j].weights[i] + learningRate * errorFL[j] * pattern.inputVector[i]; ;
                    }
                }

                dataForLearning.data.RemoveAt(index);
            }
        }


    }
}
