﻿using NAI_23.DataF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAI_23.Network
{
    public class Neuron
    {
        public double[] weights { get; set; }
        public double lastAnwser { get; set; }
        public int functionType { get; set; }
        public double lambda { get; set; }
        public Neuron(int size, int functionType, double lambda)
        {
            weights = new double[size];
            for(int i=0; i< weights.Length; i++)
            {
                weights[i] = RandomNumberGenerator.randomDouble(0, 1);
            }
            this.functionType = functionType;
            this.lambda = lambda;
        }
        public Neuron(int functionType, double lambda, double[] weigths)
        {
            this.weights = weigths;
            this.functionType = functionType;
            this.lambda = lambda;
        }
        public double calcNeuronAnwser(double[] input)
        {
            double net = calcNet(input);
            switch (functionType)
            {
                case 0:
                    //bi polarna
                    lastAnwser = 2 / (1 + Math.Exp(-lambda * net)) - 1;
                    break;
                case 1:
                    //uni polarna
                    lastAnwser = 1 / (1 + Math.Exp(-lambda * net));
                    break;
            }
            return lastAnwser;
        }
        public double calcNet(double[] input)
        {
            double net = 0;
            for(int i=0; i< weights.Length; i++)
            {
                net = net + weights[i] * input[i];
            }
            return net;
        }
        public double calcDeriv()
        {
            double result=0;
            switch (functionType)
            {
                case 0:
                    //bi polarna
                    result = 0.5 * (1 - lastAnwser * lastAnwser);
                    break;
                case 1:
                    //uni polarna
                    result = lastAnwser * (1 - lastAnwser);
                    break;
            }
            return result;
        }
    }
}
