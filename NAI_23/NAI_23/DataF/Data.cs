﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NAI_23.DataF
{
    public class Data
    {
        public IList<Pattern> data { get; set; }
        public int firstLayerSize { get; set; }
        public int secondLayerSize { get; set; }
        public int inputSize { get; set; }
        public double learningRate { get; set; }
        public double lambda { get; set; }
        public double minForOutput { get; set; }
        public double medForOutput { get; set; }
        public int functionForFirstLayer { get; set; }
        public int functionForSecondLayer { get; set; }
        public double[][] firstLayerWeights { get; set; }
        public double[][] secondtLayerWeights { get; set; }
        public double normMax { get; set; }
        public double normMin { get; set; }
        public int binary { get; set; }
        public bool networkLoaded { get; set; }
        public Data()
        {
            data = new List<Pattern>();
        }
        public Data(Data toCopy)
        {
            data = new List<Pattern>();
            firstLayerSize = toCopy.firstLayerSize;
            secondLayerSize = toCopy.secondLayerSize;
            inputSize = toCopy.inputSize;
            learningRate = toCopy.learningRate;
            lambda = toCopy.lambda;
            minForOutput = toCopy.minForOutput;
            medForOutput = toCopy.medForOutput;
            functionForFirstLayer = toCopy.functionForFirstLayer;
            functionForSecondLayer = toCopy.functionForSecondLayer;
            for (int i=0; i< toCopy.data.Count; i++)
            {
                data.Add(new Pattern(toCopy.data.ElementAt(i)));
            }
        }
        public Data(string dataFilePath)
        {
            data = new List<Pattern>();
            loadDataFromFile(dataFilePath);
            networkLoaded = false;
        }
        private void loadDataFromFile(string dataFilePath)
        {
            // Raw data
            // inputSize outputsize amount
            // data...
            try
            {
                using (StreamReader sr = new StreamReader(dataFilePath))
                {
                    String line = sr.ReadLine();
                    String[] parameters = line.Split(' ');
                    String[] inputOutput;
                    inputSize = Int32.Parse(parameters[0])+1;
                    double[] rawInput;
                    secondLayerSize = Int32.Parse(parameters[1]);
                    double[] rawOutput;
                    int amount = Int32.Parse(parameters[2]);
                    for (int i=0; i< amount; i++)
                    {
                        line = sr.ReadLine();
                        inputOutput = line.Split(' ');
                        rawInput = new double[inputSize];
                        for (int j=0; j< inputSize-1; j++)
                        {
                            rawInput[j] = Double.Parse(inputOutput[j]);
                        }
                        rawInput[inputSize - 1] = 1;
                        rawOutput = new double[secondLayerSize];
                        for (int j = 0; j < secondLayerSize; j++)
                        {
                            rawOutput[j] = Double.Parse(inputOutput[j+ inputSize-1]);
                        }
                        data.Add(new Pattern(rawInput, rawOutput));
                    }

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Błąd wczytywania pliku z danymi: "+exc.Message);
            }

        }
        public void loadNetworkFromFile(string networkFilePath)
        {
            // network config
            // inputSize firstLayerSize secondLayerSize learningRate lambda functionForFirstLayer functionForSecondLayer normMax normMin binary
            // each neuron in different line
            try
            {
                using (StreamReader sr = new StreamReader(networkFilePath))
                {
                    String line = sr.ReadLine();
                    String[] parameters = line.Split(' ');
                    String[] weightsStrings;

                    inputSize = Int32.Parse(parameters[0]);
                    firstLayerSize = Int32.Parse(parameters[1]);
                    secondLayerSize = Int32.Parse(parameters[2]);
                    learningRate = Double.Parse(parameters[3]);
                    lambda = Double.Parse(parameters[4]);
                    functionForFirstLayer = Int32.Parse(parameters[5]);
                    functionForSecondLayer = Int32.Parse(parameters[6]);
                    normMax = Double.Parse(parameters[7]);
                    normMin = Double.Parse(parameters[8]);
                    binary = Int32.Parse(parameters[9]);

                    firstLayerWeights = new double[firstLayerSize][];
                    secondtLayerWeights = new double[secondLayerSize][];

                    for (int i = 0; i < (firstLayerSize+secondLayerSize); i++)
                    {
                        line = sr.ReadLine();
                        weightsStrings = line.Split(' ');
                        
                        if(i< firstLayerSize)
                        {
                            firstLayerWeights[i] = new double[inputSize];
                            for (int j = 0; j < inputSize; j++)
                            {
                                firstLayerWeights[i][j] = Double.Parse(weightsStrings[j]);
                            }
                        }
                        else
                        {
                            secondtLayerWeights[i- firstLayerSize] = new double[firstLayerSize+1];
                            for (int j = 0; j < firstLayerSize+1; j++)
                            {
                                secondtLayerWeights[i - firstLayerSize][j] = Double.Parse(weightsStrings[j]);
                            }
                        }

                    }

                }
                networkLoaded = true;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Błąd wczytywania pliku z siecią: " + exc.Message);
            }

        }
        public void normalizeData()
        {
            double[] max = new double[2] { data[0].inputVector[0], data[0].inputVector[1] };
            double[] min = new double[2] { data[0].inputVector[0], data[0].inputVector[1] };

            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (data[i].inputVector[j] > max[j])
                    {
                        max[j] = data[i].inputVector[j];
                    }
                    else if (data[i].inputVector[j] < min[j])
                    {
                        min[j] = data[i].inputVector[j];
                    }
                }
            }
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    data[i].inputVector[j] = normalize(data[i].inputVector[j], max[j], min[j], normMax, normMin);
                }
            }
        }
        private double normalize(double value, double maxData, double minData, double normMax, double normMin)
        {
            return (value - minData) * (normMax - normMin) / (maxData - minData) + normMin;
        }
        public class Pattern
        {
            public double[] inputVector { get; set; }
            public double[] rawInputVector { get; set; }
            public double[] desiredOutput { get; set; }
            public double[] firstLayerOutput { get; set; }
            public double[] secondLayerOutput { get; set; }
            public Pattern(double[] inputVector, double[] desiredOutput, int firstLayerSize, int secondLayerSize)
            {
                this.inputVector = inputVector;
                rawInputVector = new double[inputVector.Length];
                inputVector.CopyTo(rawInputVector, 0);
                this.desiredOutput = desiredOutput;
                firstLayerOutput = new double[firstLayerSize+1];
                secondLayerOutput = new double[secondLayerSize];

            }
            public Pattern(double[] inputVector, double[] desiredOutput)
            {
                this.inputVector = inputVector;
                rawInputVector = new double[inputVector.Length];
                inputVector.CopyTo(rawInputVector, 0);
                this.desiredOutput = desiredOutput;
            }
            public Pattern(Pattern toCopy)
            {
                inputVector = new double[toCopy.inputVector.Length];
                rawInputVector = new double[toCopy.rawInputVector.Length];
                desiredOutput = new double[toCopy.desiredOutput.Length];
                firstLayerOutput = new double[toCopy.firstLayerOutput.Length];
                secondLayerOutput = new double[toCopy.secondLayerOutput.Length];

                toCopy.inputVector.CopyTo(inputVector, 0);
                toCopy.rawInputVector.CopyTo(rawInputVector, 0);
                toCopy.desiredOutput.CopyTo(desiredOutput, 0);
                toCopy.firstLayerOutput.CopyTo(firstLayerOutput, 0);
                toCopy.secondLayerOutput.CopyTo(secondLayerOutput, 0);
            }
        }
        
    }
}
