﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAI_23.DataF
{
    public class RandomNumberGenerator
    {
        public static Random rng = new Random();
        public static int randomInt(int min, int max)
        {
            return rng.Next(min, max);
        }
        public static double randomDouble(double min, double max)
        {
            return rng.NextDouble() * max + min;
        }

    }
}
