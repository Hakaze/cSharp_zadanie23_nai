﻿namespace NAI_23
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.epochCounterTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.networkInfoLabel = new System.Windows.Forms.Label();
            this.overalValidityTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.errorLimitTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.errorLimitTrainButton = new System.Windows.Forms.Button();
            this.singleEpochButton = new System.Windows.Forms.Button();
            this.changeNetworkParamButton = new System.Windows.Forms.Button();
            this.saveNetworkButton = new System.Windows.Forms.Button();
            this.networksListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.outputDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.weightsDataGridView = new System.Windows.Forms.DataGridView();
            this.setosaTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.versicolorTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.virginicaTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.outputDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.virginicaTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.versicolorTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.setosaTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.epochCounterTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.networkInfoLabel);
            this.groupBox1.Controls.Add(this.overalValidityTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.errorLimitTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.errorLimitTrainButton);
            this.groupBox1.Controls.Add(this.singleEpochButton);
            this.groupBox1.Controls.Add(this.changeNetworkParamButton);
            this.groupBox1.Controls.Add(this.saveNetworkButton);
            this.groupBox1.Controls.Add(this.networksListBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 588);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Panel kontrolny";
            // 
            // epochCounterTextBox
            // 
            this.epochCounterTextBox.Location = new System.Drawing.Point(9, 491);
            this.epochCounterTextBox.Name = "epochCounterTextBox";
            this.epochCounterTextBox.Size = new System.Drawing.Size(120, 20);
            this.epochCounterTextBox.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 475);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Ilość epok";
            // 
            // networkInfoLabel
            // 
            this.networkInfoLabel.AutoSize = true;
            this.networkInfoLabel.Location = new System.Drawing.Point(6, 525);
            this.networkInfoLabel.MaximumSize = new System.Drawing.Size(120, 0);
            this.networkInfoLabel.Name = "networkInfoLabel";
            this.networkInfoLabel.Size = new System.Drawing.Size(63, 13);
            this.networkInfoLabel.TabIndex = 12;
            this.networkInfoLabel.Text = "NETWORK";
            // 
            // overalValidityTextBox
            // 
            this.overalValidityTextBox.Location = new System.Drawing.Point(9, 335);
            this.overalValidityTextBox.Name = "overalValidityTextBox";
            this.overalValidityTextBox.Size = new System.Drawing.Size(120, 20);
            this.overalValidityTextBox.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Poprawność ogólna";
            // 
            // errorLimitTextBox
            // 
            this.errorLimitTextBox.Location = new System.Drawing.Point(9, 296);
            this.errorLimitTextBox.Name = "errorLimitTextBox";
            this.errorLimitTextBox.Size = new System.Drawing.Size(120, 20);
            this.errorLimitTextBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Min poprawność";
            // 
            // errorLimitTrainButton
            // 
            this.errorLimitTrainButton.Location = new System.Drawing.Point(9, 240);
            this.errorLimitTrainButton.Name = "errorLimitTrainButton";
            this.errorLimitTrainButton.Size = new System.Drawing.Size(120, 23);
            this.errorLimitTrainButton.TabIndex = 7;
            this.errorLimitTrainButton.Text = "Pełna nauka";
            this.errorLimitTrainButton.UseVisualStyleBackColor = true;
            this.errorLimitTrainButton.Click += new System.EventHandler(this.errorLimitTrainButton_Click);
            // 
            // singleEpochButton
            // 
            this.singleEpochButton.Location = new System.Drawing.Point(9, 211);
            this.singleEpochButton.Name = "singleEpochButton";
            this.singleEpochButton.Size = new System.Drawing.Size(120, 23);
            this.singleEpochButton.TabIndex = 6;
            this.singleEpochButton.Text = "Jedna epoka";
            this.singleEpochButton.UseVisualStyleBackColor = true;
            this.singleEpochButton.Click += new System.EventHandler(this.singleEpochButton_Click);
            // 
            // changeNetworkParamButton
            // 
            this.changeNetworkParamButton.Location = new System.Drawing.Point(9, 171);
            this.changeNetworkParamButton.Name = "changeNetworkParamButton";
            this.changeNetworkParamButton.Size = new System.Drawing.Size(120, 23);
            this.changeNetworkParamButton.TabIndex = 4;
            this.changeNetworkParamButton.Text = "Zmień parametry sieci";
            this.changeNetworkParamButton.UseVisualStyleBackColor = true;
            this.changeNetworkParamButton.Click += new System.EventHandler(this.changeNetworkParamButton_Click);
            // 
            // saveNetworkButton
            // 
            this.saveNetworkButton.Location = new System.Drawing.Point(9, 142);
            this.saveNetworkButton.Name = "saveNetworkButton";
            this.saveNetworkButton.Size = new System.Drawing.Size(120, 23);
            this.saveNetworkButton.TabIndex = 3;
            this.saveNetworkButton.Text = "Zapisz obecną sieć";
            this.saveNetworkButton.UseVisualStyleBackColor = true;
            this.saveNetworkButton.Click += new System.EventHandler(this.saveNetworkButton_Click);
            // 
            // networksListBox
            // 
            this.networksListBox.FormattingEnabled = true;
            this.networksListBox.Location = new System.Drawing.Point(9, 41);
            this.networksListBox.Name = "networksListBox";
            this.networksListBox.Size = new System.Drawing.Size(120, 95);
            this.networksListBox.TabIndex = 1;
            this.networksListBox.SelectedIndexChanged += new System.EventHandler(this.networksListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lista zapisanych sieci";
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainSplitContainer.Location = new System.Drawing.Point(157, 12);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.groupBox2);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.groupBox3);
            this.mainSplitContainer.Size = new System.Drawing.Size(796, 588);
            this.mainSplitContainer.SplitterDistance = 284;
            this.mainSplitContainer.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.outputDataGridView);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 588);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dane i wyniki";
            // 
            // outputDataGridView
            // 
            this.outputDataGridView.AllowUserToAddRows = false;
            this.outputDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.outputDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.outputDataGridView.Location = new System.Drawing.Point(6, 19);
            this.outputDataGridView.Name = "outputDataGridView";
            this.outputDataGridView.Size = new System.Drawing.Size(272, 563);
            this.outputDataGridView.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.weightsDataGridView);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(508, 588);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Wagi";
            // 
            // weightsDataGridView
            // 
            this.weightsDataGridView.AllowUserToAddRows = false;
            this.weightsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.weightsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.weightsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.weightsDataGridView.Location = new System.Drawing.Point(6, 19);
            this.weightsDataGridView.Name = "weightsDataGridView";
            this.weightsDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.weightsDataGridView.Size = new System.Drawing.Size(496, 563);
            this.weightsDataGridView.TabIndex = 0;
            // 
            // setosaTextBox
            // 
            this.setosaTextBox.Location = new System.Drawing.Point(9, 374);
            this.setosaTextBox.Name = "setosaTextBox";
            this.setosaTextBox.Size = new System.Drawing.Size(120, 20);
            this.setosaTextBox.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 358);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Poprawność Setosa";
            // 
            // versicolorTextBox
            // 
            this.versicolorTextBox.Location = new System.Drawing.Point(9, 413);
            this.versicolorTextBox.Name = "versicolorTextBox";
            this.versicolorTextBox.Size = new System.Drawing.Size(120, 20);
            this.versicolorTextBox.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 397);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Poprawność Versicolor";
            // 
            // virginicaTextBox
            // 
            this.virginicaTextBox.Location = new System.Drawing.Point(8, 452);
            this.virginicaTextBox.Name = "virginicaTextBox";
            this.virginicaTextBox.Size = new System.Drawing.Size(120, 20);
            this.virginicaTextBox.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 436);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Poprawność Virginica";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 612);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Iris network";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.outputDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.weightsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label networkInfoLabel;
        private System.Windows.Forms.TextBox overalValidityTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox errorLimitTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button errorLimitTrainButton;
        private System.Windows.Forms.Button singleEpochButton;
        private System.Windows.Forms.Button changeNetworkParamButton;
        private System.Windows.Forms.Button saveNetworkButton;
        private System.Windows.Forms.ListBox networksListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView outputDataGridView;
        private System.Windows.Forms.DataGridView weightsDataGridView;
        private System.Windows.Forms.TextBox epochCounterTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox setosaTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox virginicaTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox versicolorTextBox;
        private System.Windows.Forms.Label label6;
    }
}

