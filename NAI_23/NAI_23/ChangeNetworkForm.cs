﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NAI_23
{
    public partial class ChangeNetworkForm : Form
    {
        public Form1 mainForm { get; set; }
        public ChangeNetworkForm(Form1 mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            InitializeControls();
        }
        private void InitializeControls()
        {
            fLNeuronAmountTextBox.Text = 8 + "";
            sLNeuronAmountTextBox.Text = 3 + "";
            lambdaTextBox.Text = 1 + "";
            learningRateTextBox.Text = 0.8 + "";
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            int fLFun;
            int sLFun;
            if (fLFunBiRadioButton.Checked)
            {
                fLFun = 0;
            }
            else
            {
                fLFun = 1;
            }
            if (sLFunBiRadioButton.Checked)
            {
                sLFun = 0;
            }
            else
            {
                sLFun = 1;
            }
            mainForm.changeNetwork(Int32.Parse(fLNeuronAmountTextBox.Text), Int32.Parse(sLNeuronAmountTextBox.Text), fLFun, sLFun, Double.Parse(lambdaTextBox.Text), Double.Parse(learningRateTextBox.Text));
            this.Dispose();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void sLFunBiRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (sLFunBiRadioButton.Checked)
            {
                sLNeuronAmountTextBox.Text = 3 + "";
            }
            else
            {
                sLNeuronAmountTextBox.Text = 2 + "";
            }
        }
    }
}
