﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAI_23.DataF;
using NAI_23.Network;
using System.IO;

namespace NAI_23
{
    public partial class Form1 : Form
    {
        public TwoLayerNetwork network { get; set; }
        public Data mainData { get; set; }
        public double validity { get; set; }
        public double setosa { get; set; }
        public double versicolor { get; set; }
        public double virginica { get; set; }
        public int epochCounter { get; set; }
        bool spr = false;
        public Form1()
        {
            InitializeComponent();
            Initialize();
        }
        private void Initialize()
        {
            initializeSpliters();
            LoadPresets();
            if (networksListBox.Items.Count == 0)
            {
                mainData = new Data("DataFiles/irisBinary.txt");
                setDefaults();
                initializeOutputGrid();
                initializeNetwork();
                updateWeightsGrid();
                upDateOutputGrid();
                setNetworkInformation();
            }
            else
            {
                networksListBox.SelectedIndex = 0;
            }
            
            //
            //mainData.loadNetworkFromFile("networkFile");
            //temp
            //setDefaults();

        }
        private void setDefaults()
        {
            mainData.firstLayerSize = 8;
            mainData.secondLayerSize = 2;
            mainData.functionForFirstLayer = 0;
            mainData.functionForSecondLayer = 1;
            mainData.lambda = 1;
            mainData.learningRate = 0.8;
            mainData.normMax = 1.0;
            mainData.normMin = 0;
            mainData.binary = 1;
            epochCounter = 0;
            epochCounterTextBox.Text = epochCounter + "";
        }
        public void changeNetwork(int firstLayerSize, int secondLayerSize, int functionForFirstLayer, int functionForSecondLayer, double lambda, double learningRate)
        {

            if (functionForSecondLayer == 0)
            {
                mainData = new Data("DataFiles/irisOneToOne.txt");
                mainData.binary = 0;
            }
            else
            {
                mainData = new Data("DataFiles/irisBinary.txt");
                mainData.binary = 1;
            }
            mainData.firstLayerSize = firstLayerSize;
            mainData.secondLayerSize = secondLayerSize;
            mainData.functionForFirstLayer = functionForFirstLayer;
            mainData.functionForSecondLayer = functionForSecondLayer;
            mainData.lambda = lambda;
            mainData.learningRate = learningRate;
            mainData.networkLoaded = false;
            mainData.normMax = 1.0;
            mainData.normMin = -1.0;
            epochCounter = 0;
            epochCounterTextBox.Text = epochCounter + "";

            initializeOutputGrid();
            initializeNetwork();
            updateWeightsGrid();
            upDateOutputGrid();
            setNetworkInformation();
        }
        private void LoadPresets()
        {

            foreach (string f in Directory.GetFiles("SavedNetworks"))
            {
                networksListBox.Items.Add(f);
            }

        }
        private void updateWeightsGrid()
        {
            int maxRowForNeuron = Math.Max(network.firstLayer[0].weights.Length, network.secondLayer[0].weights.Length);
            int maxNeurons = Math.Max(network.firstLayer.Length, network.secondLayer.Length);
            weightsDataGridView.Columns.Clear();
            weightsDataGridView.Columns.Add("w1", "Warstwa ukryta");
            weightsDataGridView.Columns.Add("w2", "Warstwa wyjściowa");

            for (int i = 0; i < maxNeurons * maxRowForNeuron; i++)
            {
                weightsDataGridView.Rows.Add();
                if (i % maxRowForNeuron == 0)
                {
                    weightsDataGridView.Rows[i].HeaderCell.Value = "Neuron " + (i / maxRowForNeuron);
                }
            }
            for (int i = 0; i < network.firstLayer.Length; i++)
            {

                for (int j = 0; j < network.firstLayer[i].weights.Length; j++)
                {
                    weightsDataGridView[0, j + i * maxRowForNeuron].Value = network.firstLayer[i].weights[j];
                }
            }
            for (int i = 0; i < network.secondLayer.Length; i++)
            {

                for (int j = 0; j < network.secondLayer[i].weights.Length; j++)
                {
                    weightsDataGridView[1, j + i * maxRowForNeuron].Value = network.secondLayer[i].weights[j];
                }
            }
        }
        private void initializeSpliters()
        {
            mainSplitContainer.SplitterDistance = (int)(0.65 * this.Width);
        }
        private void initializeOutputGrid()
        {
            outputDataGridView.Columns.Clear();
            for (int i=0; i<mainData.inputSize; i++)
            {
                outputDataGridView.Columns.Add("x" + i, "X_" + i);
            }
            for (int i = 0; i < mainData.secondLayerSize; i++)
            {
                outputDataGridView.Columns.Add("d" + i, "Desired_" + i);
                outputDataGridView.Columns.Add("y" + i, "Real_" + i);
            }
        }
        private void initializeNetwork()
        {
            network = new TwoLayerNetwork(mainData);
            network.calcFirstLayerAnwser();
            network.calcSecondLayerAnwser();
        }

        private void upDateOutputGrid()
        {
            outputDataGridView.Rows.Clear();
            int correctSum = 0;
            int setosaCS = 0;
            int versicolorCS = 0;
            int virginicaCS = 0;
            for (int i = 0; i < mainData.data.Count; i++)
            {
                Data.Pattern pattern = mainData.data.ElementAt(i);
                outputDataGridView.Rows.Add();
                //double[] dataForRow = new double[mainData.inputSize + mainData.secondLayerSize * 2];
                for (int j=0; j< mainData.inputSize; j++)
                {
                    //dataForRow[j] = pattern.inputVector[j];
                    outputDataGridView[j,i].Value = pattern.rawInputVector[j];
                    
                }
                for (int j = 0; j < mainData.secondLayerSize*2; j=j+2)
                {
                    //dataForRow[j+ mainData.inputSize] = pattern.desiredOutput[j/2];
                    //dataForRow[j + mainData.inputSize+1] = pattern.secondLayerOutput[j/2];

                    outputDataGridView[j + mainData.inputSize, i].Value = pattern.desiredOutput[j/2];
                    outputDataGridView[j + mainData.inputSize + 1, i].Value = pattern.secondLayerOutput[j/2];
                    
                }

                //outputDataGridView.Rows.Add(pattern.rawInputVector[0], pattern.rawInputVector[1], pattern.rawInputVector[2], pattern.secondLayerOutput[0], pattern.desiredOutput[0], pattern.secondLayerOutput[1], pattern.desiredOutput[1], pattern.secondLayerOutput[2], pattern.desiredOutput[2]);
                //outputDataGridView.Rows.Add(dataForRow);
                bool wrong = false;

                for (int j = 0; j < pattern.secondLayerOutput.Length; j++)
                {

                    if ((pattern.secondLayerOutput[j] >= mainData.medForOutput && pattern.desiredOutput[j] == mainData.minForOutput) || (pattern.secondLayerOutput[j] < mainData.medForOutput && pattern.desiredOutput[j] != mainData.minForOutput))
                    {
                        wrong = true;
                        break;
                    }
                }
                if (wrong)
                {
                    outputDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
                else
                {
                    outputDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                    correctSum++;
                    if (mainData.binary==1)
                    {
                        if (compare(pattern.desiredOutput, new double[2] {0, 0 }))
                        {
                            setosaCS++;
                        }
                        else if (compare(pattern.desiredOutput, new double[2] { 0, 1 }))
                        {
                            versicolorCS++;
                        }
                        else
                        {
                            virginicaCS++;
                        }
                    }
                    else
                    {
                        if (compare(pattern.desiredOutput, new double[3] { 1, -1, -1 }))
                        {
                            setosaCS++;
                        }
                        else if (compare(pattern.desiredOutput, new double[3] { -1, 1, -1 }))
                        {
                            versicolorCS++;
                        }
                        else
                        {
                            virginicaCS++;
                        }
                    }
                }
            }
            validity = Math.Round((double)correctSum / (double)mainData.data.Count, 2) * 100;
            overalValidityTextBox.Text = validity + " %";
            setosa = Math.Round((double)setosaCS / (50.0) * 100,2);
            setosaTextBox.Text = setosa + " %";
            versicolor = Math.Round((double)versicolorCS / (50.0) * 100,2);
            versicolorTextBox.Text = versicolor + " %";
            virginica = Math.Round((double)virginicaCS / (50.0) * 100,2);
            virginicaTextBox.Text = virginica + " %";
        }
        private bool compare(double[] vectorA, double[] vectorB)
        {
            for(int i=0; i< vectorA.Length; i++)
            {
                if(vectorA[i]!= vectorB[i])
                {
                    return false;
                }
            }
            return true;
        }
        private void calcValidity()
        {
            network.calcFirstLayerAnwser();
            network.calcSecondLayerAnwser();
            int correctSum = 0;
            for (int i = 0; i < mainData.data.Count; i++)
            {
                Data.Pattern pattern = mainData.data.ElementAt(i);
                bool wrong = false;
                for (int j = 0; j < pattern.secondLayerOutput.Length; j++)
                {
                    if ((pattern.secondLayerOutput[j] >= mainData.medForOutput && pattern.desiredOutput[j] == mainData.minForOutput) || (pattern.secondLayerOutput[j] < mainData.medForOutput && pattern.desiredOutput[j] != mainData.minForOutput))
                    {
                        wrong = true;
                        break;
                    }
                }
                if (!wrong)
                {
                    correctSum++;
                }

            }
            validity = Math.Round((double)correctSum / (double)mainData.data.Count, 2) * 100;
        }

        private void singleEpochButton_Click(object sender, EventArgs e)
        {
            network.singleStepLerning();
            network.calcFirstLayerAnwser();
            network.calcSecondLayerAnwser();
            upDateOutputGrid();
            updateWeightsGrid();
            epochCounter++;
            epochCounterTextBox.Text = epochCounter + "";

        }

        private void errorLimitTrainButton_Click(object sender, EventArgs e)
        {
            while (validity <Int32.Parse(errorLimitTextBox.Text))
            {
                epochCounter++;
                network.singleStepLerning();
                calcValidity();
            }
            epochCounterTextBox.Text = epochCounter + "";
            spr = true;
            //network.calcFirstLayerAnwser();
            //network.calcSecondLayerAnwser();
            upDateOutputGrid();
            updateWeightsGrid();

        }

        private void evaluateButton_Click(object sender, EventArgs e)
        {

        }

        private void saveNetworkButton_Click(object sender, EventArgs e)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("SavedNetworks/network1.txt");
            // network config
            // inputSize firstLayerSize secondLayerSize learningRate lambda functionForFirstLayer functionForSecondLayer normMax normMin binary
            // each neuron in different line
            file.Write(network.allData.inputSize+" "+ network.allData.firstLayerSize + " " + network.allData.secondLayerSize + " " + network.allData.learningRate + " " + network.allData.lambda + " " + network.allData.functionForFirstLayer + " " + network.allData.functionForSecondLayer + " " + network.allData.normMax + " " + network.allData.normMin + " " + network.allData.binary);
            file.Write("\r\n");

            for (int i = 0; i <network.firstLayer.Length; i++)
            {
                for (int j = 0; j < network.firstLayer[i].weights.Length-1; j++)
                {
                    file.Write(network.firstLayer[i].weights[j]+" ");
                }

                file.Write(network.firstLayer[i].weights[network.firstLayer[i].weights.Length - 1] +"\r\n");
            }
            for (int i = 0; i < network.secondLayer.Length; i++)
            {
                for (int j = 0; j < network.secondLayer[i].weights.Length - 1; j++)
                {
                    file.Write(network.secondLayer[i].weights[j] + " ");
                }

                file.Write(network.secondLayer[i].weights[network.secondLayer[i].weights.Length - 1] + "\r\n");
            }
            file.Close();
        }

        private void networksListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (networksListBox.SelectedItem.ToString().Contains("oneToOne"))
            {
                mainData = new Data("DataFiles/irisOneToOne.txt");
            }
            else
            {
                mainData = new Data("DataFiles/irisBinary.txt");
            }
            mainData.loadNetworkFromFile(networksListBox.SelectedItem.ToString());
            initializeOutputGrid();
            initializeNetwork();
            updateWeightsGrid();
            upDateOutputGrid();
            epochCounter = 0;
            epochCounterTextBox.Text = epochCounter + "";
            setNetworkInformation();
        }
        private void setNetworkInformation()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Liczba warstw = " + 2);
            sb.AppendLine("Liczba neuronów w pierwszej warstwie = "+network.firstLayer.Length);
            if (mainData.functionForFirstLayer == 0)
            {
                sb.AppendLine("Funkcja pierwszej warstwy: bipolarna sigmoidalna");
            }
            else
            {
                sb.AppendLine("Funkcja pierwszej warstwy: unipolarna sigmoidalna");
            }
            sb.AppendLine("Liczba neuronów w drugiej warstwie = " + network.secondLayer.Length);
            if (mainData.functionForFirstLayer == 0)
            {
                sb.AppendLine("Funkcja drugiej warstwy: bipolarna sigmoidalna");
            }
            else
            {
                sb.AppendLine("Funkcja drugiej warstwy: unipolarna sigmoidalna");
            }
            sb.AppendLine("Współczynnik lambda = " + mainData.lambda);
            sb.AppendLine("Współczynnik szybkość uczenia = " + mainData.learningRate);
            if (mainData.binary == 0)
            {
                sb.AppendLine("Typ kodowania: jeden do jednego");
            }
            else
            {
                sb.AppendLine("Typ kodowania: binarnie");
            }
            sb.AppendLine("Maksimum normalizacji = " + mainData.normMax);
            sb.AppendLine("Minium normalizacji = " + mainData.normMin);
            networkInfoLabel.Text = sb.ToString();//.Replace(Environment.NewLine, "<br />");
            /*
            networkInfoLabel.Text = "Liczba warstw = " + 2 + "<br />";
            networkInfoLabel.Text = "Liczba neuronów w pierwszej warstwie = "+network.firstLayer.Length+"<br />";
            if (mainData.functionForFirstLayer == 0)
            {
                networkInfoLabel.Text = "Funkcja pierwszej warstwy: bipolarna sigmoidalna<br />";
            }
            else
            {
                networkInfoLabel.Text = "Funkcja pierwszej warstwy: unipolarna sigmoidalna<br />";
            }
            networkInfoLabel.Text = "Liczba neuronów w drugiej warstwie = " + network.secondLayer.Length + "<br />";
            if (mainData.functionForFirstLayer == 0)
            {
                networkInfoLabel.Text = "Funkcja drugiej warstwy: bipolarna sigmoidalna<br />";
            }
            else
            {
                networkInfoLabel.Text = "Funkcja drugiej warstwy: unipolarna sigmoidalna<br />";
            }
            networkInfoLabel.Text = "Współczynnik lambda = " + mainData.lambda + "<br />";
            networkInfoLabel.Text = "Współczynnik szybkość uczenia = " + mainData.learningRate + "<br />";
            if (mainData.binary == 0)
            {
                networkInfoLabel.Text = "Typ kodowania: jeden do jednego<br />";
            }
            else
            {
                networkInfoLabel.Text = "Typ kodowania: binarnie<br />";
            }
            networkInfoLabel.Text = "Maksimum normalizacji = " + mainData.normMax + "<br />";
            networkInfoLabel.Text = "Minium normalizacji = " + mainData.normMin + "<br />";
            */
        }

        private void changeNetworkParamButton_Click(object sender, EventArgs e)
        {
            ChangeNetworkForm cnf = new ChangeNetworkForm(this);
            cnf.ShowDialog();
        }
    }
}
