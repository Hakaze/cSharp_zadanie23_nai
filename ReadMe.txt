Jedno z zada� wykonane na przedmiocie Narz�dzia sztucznych inteligencji z semestru IV, jego tre�� znajduje si� w pliku tre��.txt.

Generalnie jest to w�asnor�czna implementacja dwuwarstwowej sieci neuronowej napisana w C# dla kt�rej mo�na ustawi� szereg parametr�w a nast�pnie wytrenowa�. 
Zbiorem ucz�cym jest klasyczny zbi�r irys�w. Jako, i� by�o o jedno z wielu zada� wykonanych na tym przedmiocie a nie osobny projekt 
jako taki, program jest raczej prototypem a nie sko�czon� wersj�, czyli realizuje tre�� zadania i niewiele poza tym :)
Po uruchomieniu programu zostaj� wczytane dwie wytrenowane wcze�niej sieci , w �rodkowej cz�ci okna wy�wietlaj� si� poszczeg�lne irysy i to 
jak s� klasyfikowane (zielony � dobrze, czerwony �le). Istnieje mo�liwo�� zmiany parametr�w sieci, co tworzy now� sie�, kt�r� mo�na wytrenowa�. 
Jednak�e jako , i� program nie zawiera element�w informuj�cych o post�pie uczenia, wybranie opcji �Pe�na nauka� mo�e skutkowa� bardzo d�ugim czasem oczekiwania 
lub zupe�nym brakiem odpowiedzi w przypadku kiedy stworzona sie� nigdy nie b�dzie si� w stanie odpowiednio nauczy�.
